#pragma once

/*
Copy values from the copied array, to the toCopy array.
the arrays are int pointers.
The function assume that the copied arr is small than the to copy arr.

Input: two arrays to work with, copied size.
Output: none.
*/
void copyValues(int* toCopy, int* copied, int copiedSize);
