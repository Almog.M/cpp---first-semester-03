#include "Vector.h"
#include "arrays.h"
#include <iostream>


Vector::Vector(int n)
{
	if (n < MIN_VECTOR_CAPACITY)
		n = MIN_VECTOR_CAPACITY;
	this->_capacity = n;
	this->_size = n;
	this->_resizeFactor = n;
	this->_elements = new int[this->_capacity];
}

Vector::~Vector()
{
	if (this->_elements) //If the _elements is not null.
	{
		delete[] this->_elements;
		this->_elements = nullptr;
	}
	
}

int Vector::size()const
{
	return this->_size;
}

int Vector::capacity() const
{
	return this->_capacity;
}

int Vector::resizeFactor() const
{
	return this->_resizeFactor;
}

bool Vector::empty() const
{
	return !(bool(this->_size));
}

void Vector::addCapacity()
{
	int* temp = this->_elements; //For deleting.
	this->_elements = new int[this->_capacity + this->_resizeFactor];

	copyValues(temp, this->_elements, this->_size);
	this->_capacity += this->_resizeFactor;

	if (temp != this->_elements)
	{
		delete[] temp;
		temp = nullptr;
	}
}

void Vector::deepCopy(const Vector & other)
{
	this->_capacity = other._capacity;
	this->_size = other._size;
	this->_resizeFactor = other._resizeFactor;
	this->_elements = new int[this->_capacity];

	for (int i = 0; i < this->_capacity; i++)
	{
		this->_elements[i] = other._elements[i];
	}
}

void Vector::push_back(const int & val)
{
	if (this->_elements)
	{
		if (this->_size == this->_capacity)
		{
			addCapacity();
		}
		this->_size++;
		this->_elements[this->_size - 1] = val;
	}
	else
	{
		std::cout << VECTOR_NOT_EXIST_MESSAGE << std::endl;
	}
}

int Vector::pop_back()
{
	int lastElement = POP_EMPTY_VECTOR_VALUE;
	if (this->_elements)
	{
		if (this->_size > 0)
		{
			lastElement = this->_elements[this->_size - 1];
			this->_size--; //Removes the last element.
		}
		else
		{
			std::cerr << "error: pop from empty vector." << std::endl;
		}
	}
	else
	{
		std::cerr << VECTOR_NOT_EXIST_MESSAGE << std::endl;
	}
	
	return lastElement;
}

void Vector::reserve(int n)
{
	while(this->_capacity < n)
	{
		addCapacity();
	}
}

void Vector::resize(int n)
{
	if (n > this->_capacity)
	{
		reserve(n);
	}
	this->_size = n;
}

void Vector::assign(int val)
{
	int i = 0;
	if (this->_elements)
	{
		for (i = 0; i < this->_size; i++)
		{
			this->_elements[i] = val;
		}
	}
	else
	{
		std::cerr << VECTOR_NOT_EXIST_MESSAGE << std::endl;
	}
	
}

void Vector::resize(int n, const int & val)
{
	int i = 0;
	int oldSize = this->_size;
	
	resize(n);
	for (i = oldSize; i < this->_size; i++)
	{
		this->_elements[i] = val;
	}
}

Vector::Vector(const Vector & other)
{
	deepCopy(other);
}

Vector & Vector::operator=(const Vector & other)
{
	int* temp = this->_elements;
	deepCopy(other);
	if (temp != this->_elements)
		delete[] temp;
	return *this;
}

int & Vector::operator[](int n) const
{
	int* element = NULL;
	if (this->_elements && !this->empty()) //If the vector is exist. 
	{
		if ((n < this->_size) && (n >= 0)) //If the index valid.
		{
			for (int i = 0; i <= n; i++)
			{
				element = this->_elements + i;
			}
		}
		else
		{
			std::cerr << "index too long or negative" << std::endl; 
			element = this->_elements;
		}
	}
	else
	{
		std::cerr << VECTOR_NOT_EXIST_MESSAGE << std::endl;
		_exit(1);
	}
	


	return *element;
}
