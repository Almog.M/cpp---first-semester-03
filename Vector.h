#ifndef VECTOR_H
#define VECTOR_H

#define MIN_VECTOR_CAPACITY 2
#define POP_EMPTY_VECTOR_VALUE -9999
#define VECTOR_NOT_EXIST_MESSAGE "The vector is not exist."
#define NOTHING_IN_VECTOR_VALUE -1 

class Vector
{
private:
	//Fields
	int* _elements;
	int _capacity; //Total memory allocated
	int _size; //Size of vector to access
	int _resizeFactor; // how many cells to add when need to reallocate

	//Wide the capacity of the vector - the field and the space, keeping on the values that were before the adding of the capacity.
	void addCapacity();
	void deepCopy(const Vector& other); //Doing deep copy.

public:
	
	//A
	Vector(int n); //Builds the capacity, size, _resizeFactor in the n value.
	~Vector(); //Destructor...
	int size() const;//return size of vector
	int capacity() const;//return capacity of vector
	int resizeFactor() const; //return vector's resizeFactor
	bool empty() const; //returns true if size = 0

	//B
	//Modifiers
	void push_back(const int& val);//adds element at the end of the vector
	int pop_back();//removes and returns the last element of the vector
	void reserve(int n);//change the capacity
	void resize(int n);//change _size to n, unless n is greater than the vector's capacity
	void assign(int val);//assigns val to all elements
	void resize(int n, const int& val);//same as above, if new elements added their value is val

	//C
	//The big three (d'tor is above)
	Vector(const Vector& other);
	Vector& operator=(const Vector& other);

	//D
	//Element Access
	int& operator[](int n) const;//n'th element if vector empty, returns -1, if n bigger than the size -1, returns the first element - in this case the vector is not empty.



};

#endif