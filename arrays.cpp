#include "arrays.h"

void copyValues(int* toCopy, int* copied, int copiedSize)
{
	int i = 0;
	if (toCopy != copied)
	{
		for (i = 0; i < copiedSize; i++)
		{
			toCopy[i] = copied[i];
		}
	}
}